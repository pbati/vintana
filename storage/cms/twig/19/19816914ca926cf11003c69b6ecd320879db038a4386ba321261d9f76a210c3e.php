<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /Users/vinta/Sites/vintana/themes/demo/partials/Artist.htm */
class __TwigTemplate_1228da46b544be7dda0691fe004195f7a93d453416164266e1cee0d0711ed65e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo twig_escape_filter($this->env, ($context["artist"] ?? null), "html", null, true);
    }

    public function getTemplateName()
    {
        return "/Users/vinta/Sites/vintana/themes/demo/partials/Artist.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{# Get product item #}
{{artist}}", "/Users/vinta/Sites/vintana/themes/demo/partials/Artist.htm", "");
    }
}
