<?php namespace Vinta\Vintana\Models;

use Model;

/**
 * Model
 */
class Artist extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'vinta_vintana_artists';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsToMany = [
        'product' => [
            \Lovata\Shopaholic\Models\Product::class,
            'table' => 'vinta_vintana_artists_of_products'
        ]
    ];

    public $attachOne = [
        'images' => 'System\Models\File'
    ];
}
