<?php namespace Vinta\Vintana;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    /**
     * @return array
     */
    public function registerListColumnTypes()
    {
        return [
            // A local method, i.e $this->evalUppercaseListColumn()
            'uppercase' => [$this, 'evalUppercaseListColumn'],

            // Using an inline closure
            'loveit' => function($value) { return 'I love '. $value; }
        ];
    }

    public function evalUppercaseListColumn($value, $column, $record)
    {
        if($record->is_featured)
            return 'Yes';
        return '';
    }
}
