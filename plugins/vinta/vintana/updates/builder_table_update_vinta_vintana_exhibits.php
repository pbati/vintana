<?php namespace Vinta\Vintana\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVintaVintanaExhibits extends Migration
{
    public function up()
    {
        Schema::table('vinta_vintana_exhibits', function($table)
        {
            $table->text('description')->default('null')->change();
            $table->renameColumn('exhibits', 'name');
        });
    }
    
    public function down()
    {
        Schema::table('vinta_vintana_exhibits', function($table)
        {
            $table->text('description')->default('NULL')->change();
            $table->renameColumn('name', 'exhibits');
        });
    }
}
