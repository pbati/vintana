<?php namespace Vinta\Vintana\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVintaVintanaArtists extends Migration
{
    public function up()
    {
        Schema::table('vinta_vintana_artists', function($table)
        {
            $table->unique('name');
        });
    }
    
    public function down()
    {
        Schema::table('vinta_vintana_artists', function($table)
        {
             $table->dropUnique('name');
        });
    }
}
