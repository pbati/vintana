<?php namespace Vinta\Vintana\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVintaVintanaArtists extends Migration
{
    public function up()
    {
        Schema::create('vinta_vintana_artists', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('description');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vinta_vintana_artists');
    }
}
