<?php namespace Vinta\Vintana\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVintaVintanaArtists2 extends Migration
{
    public function up()
    {
        Schema::table('vinta_vintana_artists', function($table)
        {
            $table->text('description')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('vinta_vintana_artists', function($table)
        {
            $table->text('description')->nullable(false)->change();
        });
    }
}
