<?php namespace Vinta\Vintana\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVintaVintanaGenres extends Migration
{
    public function up()
    {
        Schema::table('vinta_vintana_genres', function($table)
        {
            $table->unique('name');
        });
    }
    
    public function down()
    {
        Schema::table('vinta_vintana_genres', function($table)
        {
            $table->dropUnique('name');
        });
    }
}
