<?php namespace Vinta\Vintana\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVintaVintanaArtists4 extends Migration
{
    public function up()
    {
        Schema::table('vinta_vintana_artists', function($table)
        {
            $table->text('description')->default('null')->change();
            $table->string('image', 191)->default('null')->change();
        });
    }
    
    public function down()
    {
        Schema::table('vinta_vintana_artists', function($table)
        {
            $table->text('description')->default('NULL')->change();
            $table->string('image', 191)->default('NULL')->change();
        });
    }
}
