<?php namespace Vinta\Vintana\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVintaVintanaGenresOfProducts extends Migration
{
    public function up()
    {
        Schema::create('vinta_vintana_genres_of_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('genre_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['genre_id', 'product_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vinta_vintana_genres_of_products');
    }
}
