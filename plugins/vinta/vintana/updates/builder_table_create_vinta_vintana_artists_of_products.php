<?php namespace Vinta\Vintana\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVintaVintanaArtistsOfProducts extends Migration
{
    public function up()
    {
        Schema::create('vinta_vintana_artists_of_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('product_id')->unsigned();
            $table->integer('artist_id')->unsigned();
            $table->primary(['product_id', 'artist_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vinta_vintana_artists_of_products');
    }
}
