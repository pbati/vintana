<?php namespace Vinta\Vintana\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVintaVintanaExhibits2 extends Migration
{
    public function up()
    {
        Schema::table('vinta_vintana_exhibits', function($table)
        {
            $table->unique('name');
        });
    }
    
    public function down()
    {
        Schema::table('vinta_vintana_exhibits', function($table)
        {
            $table->dropUnique('name');
        });
    }
}
