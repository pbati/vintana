<?php namespace Vinta\Vintana\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVintaVintanaArtists3 extends Migration
{
    public function up()
    {
        Schema::table('vinta_vintana_artists', function($table)
        {
            $table->string('image')->nullable();
            $table->text('description')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('vinta_vintana_artists', function($table)
        {
            $table->dropColumn('image');
            $table->text('description')->default('NULL')->change();
        });
    }
}
