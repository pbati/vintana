<?php namespace Vinta\Vintana\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVintaVintanaExhibits3 extends Migration
{
    public function up()
    {
        Schema::table('vinta_vintana_exhibits', function($table)
        {
            $table->boolean('is_featured')->default(0);
            $table->text('description')->default('null')->change();
        });
    }
    
    public function down()
    {
        Schema::table('vinta_vintana_exhibits', function($table)
        {
            $table->dropColumn('is_featured');
            $table->text('description')->default('NULL')->change();
        });
    }
}
