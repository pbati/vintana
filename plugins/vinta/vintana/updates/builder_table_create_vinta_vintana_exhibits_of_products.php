<?php namespace Vinta\Vintana\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVintaVintanaExhibitsOfProducts extends Migration
{
    public function up()
    {
        Schema::create('vinta_vintana_exhibits_of_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('exhibit_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['exhibit_id', 'product_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vinta_vintana_exhibits_of_products');
    }
}
