<?php return [
    'plugin' => [
        'name' => 'Vintana',
        'description' => '',
    ],
    'field' => [
        'name' => 'Name',
        'description' => 'Description',
    ],
    'name' => 'name',

    'tab' => [
        'artist' => 'Artists',
        'exhibit' => 'Exhibits',
        'genre' => 'Genres',
    ],
];